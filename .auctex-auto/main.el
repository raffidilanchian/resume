(TeX-add-style-hook
 "main"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("altacv" "10pt" "a4paper" "ragged2e" "withhyper")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("roboto" "rm") ("lato" "defaultsans")))
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "href")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "altacv"
    "altacv10"
    "paracol"
    "roboto"
    "lato")
   (LaTeX-add-bibliographies
    "sample")
   (LaTeX-add-xcolor-definecolors
    "SlateGrey"
    "LightGrey"
    "Teal200"
    "Cyan900"
    "Cyan500"
    "name"
    "tagline"
    "heading"
    "headingrule"
    "subheading"
    "accent"
    "emphasis"
    "body"))
 :latex)

